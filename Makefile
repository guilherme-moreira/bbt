MKDIR ?= mkdir -p

SRCS := $(wildcard ./slides/*.md)
RENDERED := $(SRCS:%.md=dist/%.html)

HEADER := resources/slides-css.html

.PHONY: all
all: $(RENDERED) dist/index.html

.PHONY: help
help:
	@echo "all          build the index page and all presentations"
	@echo "checkdeps    check if hard and soft dependencies are available"
	@echo "clean        clean build files"
	@echo "deploy-prod  deploy built files to production"
	@echo "dev          start local development server"
	@echo "format       format shell scripts with shfmt"
	@echo "help         show this message"
	@echo "lint         lint shell scripts with shellcheck and shfmt"

.PHONY: clean
clean:
	rm -rf dist

.PHONY: checkdeps
checkdeps: ./scripts/check-deps.sh
	./scripts/check-deps.sh

.PHONY: dev
dev: all
	wrangler pages dev dist --live-reload

.PHONY: format
format: ./scripts/format.sh
	./scripts/format.sh

.PHONY: lint
lint:
	./scripts/lint.sh

.PHONY: deploy-prod
deploy-prod: ./scripts/deploy.sh
	PROJECT_BRANCH=prod ./scripts/deploy.sh	

dist/index.html: $(SRCS) ./scripts/build-index.sh
	@mkdir -p dist
	./scripts/build-index.sh > $@

dist/%.html: %.md $(HEADER)
	@mkdir -p dist/slides
	pandoc -t revealjs $< \
		-o $@ \
		--standalone \
		--include-in-header=$(HEADER) \
		--slide-level=0
