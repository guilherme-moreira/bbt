# BBT

Esse repositório contém o código fonte para as apresentações do Bate-Bola
Técnico.

As apresentações são escritas em Markdown e convertidas para HTML usando
[`pandoc`](https://pandoc.org).

## Dependências

O projeto deve ser executado em um sistema GNU/Linux, e as seguintes
dependências devem estar instaladas:

- `make`: ferramenta de build (`sudo apt install make`)
- `bash`: shell scripting (provavelmente já instalado, mas `sudo apt install
  bash`)
- `pandoc`: conversão de `.md` para `.html` (`sudo apt install pandoc`)
- `jq`: manipulação de json (`sudo apt install jq`)

Além dessas, algumas dependências são opcionais:

- `shellcheck`: linter para scripts `sh` (`sudo apt install shellcheck`)
- `shfmt`: formatter para scripts `sh` (`sudo apt install shfmt`)
- `wrangler`: deploy do projeto no _Cloudfare Pages_ ([instruções de
  instalação](https://developers.cloudflare.com/workers/wrangler/install-and-update/))

Para checkar se as dependências estão disponíveis:

```sh
make checkdeps # ou ./scripts/check-deps.sh
```

## Construindo as Apresentações

Para construir todas as apresentações:

```sh
make all
```

Para construir uma apresentação específica:

```sh
make dist/slides/<apresentação>.html 
# onde <apresentação> corresponde à um arquivo slides/<apresentacao>.md
```
