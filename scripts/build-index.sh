#!/usr/bin/env bash
set -euo pipefail

function make_list_item() {
  title="$1"
  pub_date="$2"
  authors="$3"
  filename="$4"

  cat <<EOF
<li>
  <a href="${filename%.*}.html">$title</a> &mdash; 
  <small>
    Elaborado por $authors em <time datetime="$pub_date">$(date +'%d/%m/%Y' -d"$pub_date")</time>
  </small>
</li>
EOF
}

function make_list() {
  sorted_presentations=$(for f in slides/*.md; do
    pandoc --template=resources/json-meta.tpl "$f" |
      jq --arg FILENAME "$f" '{title, date, author, filename: $FILENAME}'
  done | jq -s 'sort_by(.date)')

  jq -r '.[] | [.title, .date, (.author | join(", ")), .filename] | @tsv' <<<"$sorted_presentations" |
    while IFS=$'\t' read -r title pub_date filename authors; do
      make_list_item "$title" "$pub_date" "$filename" "$authors"
    done
}

cat <<EOF
<!DOCTYPE html>
<html>
  <head>
    <title>BBT - Bate Bola Técnico</title>
    <style>
      html {
        font-size: 1.5;
        font-family: -system-ui, sans-serif;
      }

      body {
        min-height: 100vh;
        max-width: 800px;
        margin-inline: auto;
        padding-inline: 1rem;

        background-color: #000;
        color: #d9d9d9;
      }

      ul {
        display: flex;
        flex-direction: column;
        row-gap: 1rem;
      }

      ul.reverse {
        flex-direction: column-reverse;
      }

      a {
        color: #44c7ff;
      }

      a:visited {
        color: #e9579e;
      }
    </style>

  </head>
  <body>
    <h1>Lista de Apresentações do BBT</h1>

    <ul>
      $(make_list)
    </ul>
  </body>
</html>
EOF
