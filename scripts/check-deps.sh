#!/usr/bin/env bash
set -euo pipefail

hard_deps='make bash pandoc jq'
soft_deps='shellcheck shfmt wrangler'

function check_deps() {
  local err=0
  read -r -a deps <<<"$1"

  for dep in "${deps[@]}"; do
    if ! command -v "$dep" &>/dev/null; then
      echo "cannot find $dep"
      ((err++))
    fi
  done

  return "$err"
}

echo "checking hard dependencies"
check_deps "$hard_deps" && echo "ok"

echo "checking soft dependencies"
check_deps "$soft_deps" && echo "ok"
