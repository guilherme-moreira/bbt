#!/usr/bin/env bash
set -euo pipefail

PROJECT_NAME=${PROJECT_NAME:-bbt}

wrangler pages deploy dist \
  --project-name "$PROJECT_NAME" \
  --branch "$PROJECT_BRANCH"
