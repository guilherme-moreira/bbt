#!/usr/bin/env bash
set -euo pipefail

script_dir="$(dirname -- "$(readlink -f -- "${BASH_SOURCE[0]}")")"
cd "$script_dir" || exit 1

for file in ./*.sh; do
  shfmt -i 2 -s -l -w "$file"
done
