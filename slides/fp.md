---
title: Programação Funcional
theme: simple
author:
  - Guilherme Puida
date: 2023-07-27
---

## Diferentes paradigmas

- Imperativo: Assembly
- Procedural: ALGOL, C, Fortran
- Orientado à Objetos: Smalltalk, Java
- Declarativo: SQL, HTML, CSS
- Lógico: Prolog, Datalog
- **Funcional**

---

## Uma breve história

- Cálculo Lambda.
- _LISP_ de McCarthy. 
- Java 8+.
- Haskell, Elixir, Scheme, Haskell, Idris, Agda, ...

---

## O que é

- Programas compostos por composição de funções.
- Minimização de efeitos colaterais.
- Declarativo, não imperativo.

---

## Composição de funções

- Funções são cidadãos de primeira classe.
- Ou seja: **são valores como quaisquer outros.**
- Permite que sejam declaradas em escopo local, passadas como parâmetros ou
  como valores de retorno, etc.

---

## Minimização de efeitos colaterais

- Operam somente com os argumentos que foram passados.
- Não conseguem afetar o ambiente (inclui banco de dados, requisições HTTP e
  até _printar_ no console).

---

## A importância de funções puras

- Permitem que comportamentos sejam observados de maneira isolada.
- **Mais fácil de testar.**
- **Mais fácil de provar que uma implementação é correta.**

---

Mas se é impossível se comunicar com o ambiente, como fazer qualquer coisa útil?

---

**Usando funções impuras :(**

Mas minimizando a quantidade e o escopo delas.

Núcleo funcional, casca imperativa.

---

## Nós já usamos programação funcional

- JavaScript: `Array.map`, `Array.filter`, `Array.reduce`, etc.
- Java 8+: Streams e Lambdas.
- Kotlin: sintaxe dedicada para chamada de funções anônimas `{ it -> ... }`.

---

## Muito software é funcional

- Nubank: Clojure, Scala.
- Twitter: Scala.
- Walmart: Scala.
- Whatsapp: Erlang.
- Pinterest: Elixir.
- Jane Street: Haskell, OCaml.
- SoundCloud: Clojure, Scala.
- CircleCI: Clojure.
- Metabase: Clojure.
- Pandoc: Haskell.

---


## Exemplos

---

Soma de números em um array.

```js
function soma(array) {
  let s = 0;
  for (let i = 0; i < array.length(); i++) {
    s += array[i];
  }
  return s;
}
```

```js
const soma = (array) => array.reduce((a, b) => a + b, 0);
```

---

Contar quantidade de linhas em um texto.

```js
function linhas(texto) {
  let l = 0;
  for (let i = 0; i < texto.length(); i++) {
    if (texto[i] === '\n') {
      l++;
    }
  }
  return l;
}
```

```js
const linhas = (texto) => texto.filter(c => c === '\n').length();
```

---

## Um exemplo real!

```ts
// src/framework/converter/converter.ts
export interface Converter<I, O> {
  convert(input: I): O
}
```

```ts
type ConverterFn<I, O> = (input: I): O
```

---

```ts
export class DateConverter implements Converter<Date | string | number, string> {
  convert(input: Date | string | number): string {
    if (!input) return '';
    let dateTime: DateTime;
    if (typeof input === 'string') {
      dateTime = DateTime.fromISO(input);
    } else if (typeof input === 'number') {
      dateTime = DateTime.fromSeconds(input as number);
    } else if (input instanceof Date) {
      dateTime = DateTime.fromJSDate(input);
    } else {
      throw new Error('Invalid input type. Expecting a string or a Date object.');
    }

    return dateTime.toFormat('dd/MM/yyyy');
  }
}
```

```ts
const parseDateTime = (input: Date | string | number): DateTime => {
  if (typeof input === 'string') {
    return DateTime.fromISO(input);
  } else if (typeof input === 'number') {
    return DateTime.fromSeconds(input as number);
  } else if (input instanceof Date) {
    return DateTime.fromJSDate(input);
  } else {
    throw new Error('Invalid input type. Expecting a string or a Date object.');
  }
}

const dateConverter: ConverterFn<Date | string | number, string> = (input) => {
  if (!input) return '';
  return parseDateTime(input).toFormat('dd/MM/yyyy');
}
```

---

## Podemos fazer melhor!

---

```ts
const createDateConverter = (format: string): ConverterFn<Date | string | number, string> => 
  (input) => {
    if (!input) return '';
    return parseDateTime(input).toFormat(format);
  }

const dateConverter = createDateConverter('dd/MM/yyyy');
const dateHourConverter = createDateConverter('dd/MM/yyyy HH:mm');
const bestDateConverter = createDateConverter('yyyy/MM/dd');
```

**Removemos código duplicado!**

---

Essa estratégia de criar funções "geradoras" que retornam funções mais
específicas é chamado de **aplicação parcial** ou **Currying**.

---

```ts
const applyOr = <I, O>(fn1: ConverterFn<I, O>, fn2: ConverterFn<I, O>): ConverterFn<I, O> => 
  (input) => {
    const result = fn1(input);
    return Boolean(result) ? result : fn2(input);
  }

const dateConverter = createDateConverter('dd/MM/yyyy');
const dateConv1 = applyOr(dateConverter, (_) => '--');
const dateConv2 = applyOr(dateConverter, (_) => 'SEM DATA');
```

---

Um problema com essa implementação é que é difícil saber quando uma operação
falhou. Isso é importante para aplicar as operações subsequentes.

---

Algumas linguagens funcionais resolvem esse problema com _Functors_, _Applicatives_ e
_Monads_. São conceitos relacinados à _Teoria das Categorias_, que é uma área
da Matemática.

---

`java.util.Optional` tem uma interface funcional nos métodos `map`, `flatMap`,
`filter`, etc.

Os tipos nulláveis do Kotlin (`T?`) também possuem interface semelhante.

---

## FIM!

- [Código no TS Playground](https://www.typescriptlang.org/play?#code/CYUwxgNghgTiAEkoGdnwCJQC4gCoEsBbBAbwCh5L5ktt8x4AzGAe0IEkBlAeQAp8AdgAcArlgBc1LDEEBzAJSTMOAsQDcFKjToNmbTuBYDgyfsLGSBIwgCMQMRRmx4iIDVSk6mrQgClOyiBmohJOOI6Bqm6alFgsAGIsMITYvIxJKaE0MgIKktlyGgC+ZGRYAJ5CCADCRgBu9jgw8QIAPOwANPDcAHzwALzwwRbw7PIDfdylYEY08EKwyCCRrgNDgiFKzvAAPlI5srvwVrb2Ec5RE-DkHviMQxVVLPcbYgP9gwDkBbmf4zceKhwLAiGACMIuYgAOj0HB4wyw8ncVCK8BAECW8DuD0qIGeWPMWHeXxOdhgf2uMUBwNB4JW0NhBhmxlMr0RyMoqPRmOxCIJ2gEYDx90C-ypHhpYIhURhPn8gQRSKpXIxpHFVCwAAtWAB3Y4gPUAURgrBgvE+7AEdQAhxB8MAWH8OfASiUyMy5sBnLUrY17AB9SQ+howJotVqBI4-Q57Un2LrRvqDRVXAGUXkAQjZ40l4M+n2dufmi2WF1ciqhcUSyVSn2AwAA9ABZJsN8rt8pOshuj1EsBwZyBYN+mBrNIZbD5aRyRzD0P2cORvbRo5xmAJ6e5JN9FP9PpprH3XhZwk5kAgqX5wvn2nFmBLelBbOVhITrDjmvs7vTWZEr04OcmjWfsQEHb16hHc162bVsO07JVe3gf8QAACRYUFgIHHAhwg+czTrRsWzbDt4BQlDxEIQgu0QkRkCgHDfTwzDQOw8DGKac0iOguCu3dX94CgIQhAgcpuFHQZ2i6Xo0gEABGINcLDNpOm6HoukYAQACYFPYhdlKknpZ0UvTJNUiYqV3fd1UoRC4GQEQICJQYNNkxVnQlG8pQAIRYFgIFAgReDshzEXgAB+eBgsc+BJA0zS3OVH8BE9Ni6lktZBOE0SzWQwD4yGf1xj3eBPgAWlK6j+Ny+pNIyoSRLE3hqt09cCqKvpPk4Q0mwwABBXBeqdIA)
