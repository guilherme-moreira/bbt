---
title: Git - The Stupid Content Tracker
theme: simple 
author:
  - Guilherme Puida
date: 2023-06-09
---

```bash
$ man git | grep ^NAME$ -A 1
NAME
       git - the stupid content tracker
```
---

## Origem - Kernel do Linux

* 1991 - 2002: Patches por email.
* 2002 - 2005: BitKeeper.
* 2005 - agora: git.

---

## Motivação

1. FOSS - _Free and Open Source Software_.
2. Distribuído.
3. Rápido.
4. Escalável.

---

## Diferenças para outros VCSs 

1. Quase todas as operações são realizadas localmente.
2. Usa _snapshots_ em vez de _deltas_.
3. Integridade referencial, (quase) impossível perder informações.
 
---

## Porcelana e Encanamento

Os comandos do `git` são divididos em duas categorias:

* Porcelana: comandos de alto nível, para serem usados no dia-a-dia. 
  * `add/diff/commit/push/pull/merge/...`
* Encanamento: comandos de baixo nível, usados para implementar a porcelana.
  * `merge-file/hash-object/update-ref/mktree/...`

---

## _Workflow_ Básico

![](https://git-scm.com/book/en/v2/images/areas.png)

---

1. `pull`
2. `add`
3. `commit`
4. `push`

---

## Boas Práticas

---

### Commits Atômicos

Princípio da responsabilidade única. 

---

### Boas Mensagens de Commit 

---

### Código sempre _buildável_

útil quando usando `git bisect`.

---

## Comandos úteis 

### (mas pouco usados)

---

### `git reflog`

Log de todas as ações realizadas no repositório local.
Útil quando algo quebra e você não sabe resolver.

`man git-reflog`

---

### `git reset/restore/revert`

* `reset`: para atualizar a branch, adicionando ou removendo commits. Essa ação
  altera o histórico de commits.
* `restore`: restaurar arquivos da `working tree` com os conteúdos do `index`
  ou de outro commit/branch/tag. Essa ação não atualiza sua branch nem altera o
  histórico de commits.
* `revert`: faz um novo commit que reverte as mudanças realizadas por outro
  commit. 

```bash
man git | grep "Reset, restore and revert" -A 10
```

---

### `git bisect`

Busca binária para encontrar o commit que introduziu um bug.

`man git-bisect`

---

### `git stash`

Guarda as modificações atuais e volta para o estado limpo da árvore.

`man git-stash`

---

## Meus commandos mais usados

1. `git add --patch`
2. `git commit --verbose`
3. `git status --short`
4. `git switch -`
5. `git restore --staged`
6. `git show`
7. `git rebase --interactive`

---

## Mais recursos 

* [Livro Oficial: git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)
* [Oh Shit Git!: ohshitgit.com/pt_BR](https://ohshitgit.com/pt_BR)
* [Learn Git Branching: learngitbranching.js.org](https://learngitbranching.js.org)
